import QtQuick 2.0
import QtQuick.Controls 2.12

import ShowItemsType 1.0
import Style 1.0

import "../Components"
import "../Style"

Drawer {
    id: root

    property color color

    signal showNotes(var type);
    signal manageCategories();

    background: Rectangle {
        color: root.color
        radius: 15

        Rectangle {
            width: parent.radius
            height: parent.height
            color: root.color
        }
    }

    ToolButton {
        id: gearButton
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 20

        icon.source: Assets.gear
    }

    ScrollView {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: gearButton.bottom
        anchors.bottom: parent.bottom
        anchors.topMargin: 30
        anchors.bottomMargin: 20
        clip: true

        width: parent.width * 0.8

        Column {
            spacing: 10
            MenuItem {
                text: "All notes"
                icon.source: Assets.notes
                onTriggered: {
                    root.showNotes(ShowItemsType.AllNotes);
                }
            }
            MenuItem {
                text: "Favourites"
                icon.source: Assets.favorite
                onTriggered: {
                    root.showNotes(ShowItemsType.Favorites);
                }
            }
            MenuItem {
                text: "Locked notes"
                icon.source: Assets.locked
                onTriggered: {
                    root.showNotes(ShowItemsType.Locked);
                }
            }
            MenuItem {
                text: "Frequently used"
                icon.source: Assets.bookmark
                onTriggered: {
                    root.showNotes(ShowItemsType.FrequentlyUsed);
                }
            }
            MenuItem {
                text: "Shared notebooks"
                icon.source: Assets.share
                onTriggered: {
                    root.showNotes(ShowItemsType.SharedNotes);
                }
            }
            MenuItem {
                text: "Recycle bin"
                icon.source: Assets.delIcon
                onTriggered: {
                    root.showNotes(ShowItemsType.RecycleBin);
                }
            }

            MenuSeparator {}

            Repeater {
                model: CategoriesStore.categories

                MenuItem {
                    text: modelData.name
                }
            }

            Button {
                width: parent.width

                text: "Manage categories"
                onClicked: root.manageCategories()
            }
        }
    }
}
