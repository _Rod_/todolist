import QtQuick 2.0
import QtQuick.Controls 2.12
import Style 1.0
import QtQuick.Controls.Styles 1.4

import "../../Components"
import "../../Style"

ToolBar {
    id: root

    property bool isFavorite: false
    property bool isLocked: false

    signal back();
    signal edit();
    signal share();
    signal favorite();
    signal remove();
    signal lock();

    Rectangle {
        anchors.fill: parent

        id: background
        color: "red"
        opacity: 0.5
        visible: false
    }


    ToolButton {
        id: backButton

        anchors.margins: 10
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter

        icon.source: Assets.back

        onClicked: root.back()
    }


    ToolButton {
        id: editButton

        anchors.margins: 10
        anchors.right: favoriteButton.left
        anchors.verticalCenter: parent.verticalCenter

        icon.source: Assets.editIcon
        onClicked: root.edit();
    }

    ToolButton {
        id: favoriteButton

        anchors.margins: 10
        anchors.right: shareButton.left
        anchors.verticalCenter: parent.verticalCenter

        icon.source: root.isFavorite ? Assets.favorite : Assets.nonfavorite
        onClicked: root.favorite();
    }

    ToolButton {
        id: shareButton

        anchors.margins: 10
        anchors.right: moreButton.left
        anchors.verticalCenter: parent.verticalCenter

        icon.source: Assets.share

        enabled: false
        onClicked: root.share();
    }


    ToolButton {
        id: moreButton

        anchors.margins: 10
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter

        icon.source: Assets.more

        onClicked: {
            contextMenu.x = moreButton.x
            contextMenu.y = moreButton.y;

            contextMenu.open();
        }
    }

    Menu {
        id: contextMenu

        MenuItem {
            text: 'delete'
            icon.source: Assets.delIcon
            onTriggered: root.remove();
        }
        MenuItem {
            text: root.isLocked ? 'unlock' : 'lock'
            icon.source: root.isLocked ? Assets.unlocked : Assets.locked
            onTriggered: root.lock();
        }
        MenuItem {
            enabled: false
            text: 'send to reminder'
        }
        MenuItem {
            enabled: false
            text: 'print'
        }
        MenuItem {
            enabled: false
            text: 'export to PDF'
        }
    }
}
