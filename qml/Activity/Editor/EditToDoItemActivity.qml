import QtQuick 2.12
import QtQuick.Controls 2.12
import Style 1.0

import "../../Components"
import "../../Style"
import "../Category"

Item {
    id: root

    property color textFieldBgColor: "#E0E0E0"
    property var item
    property int categoryId: item ? item.category : 0

    signal saveItem (var category, var title, var text);
    signal cancel();
    signal remove();
    signal favorite();
    signal lock();

    TopToolbar {
        id: topToolbar

        height: Sizes.topToolbarHeight
        width: parent.width

        anchors.left: parent.left
        anchors.top: parent.top

        isFavorite: item ? item.favorite : false
        isLocked: item ? item.locked : false

        onBack: {
            root.cancel();
        }

        onEdit: {
            textEdit.forceActiveFocus()
        }

        onLock: {
            root.lock();
        }

        onRemove: {
            root.remove();
        }

        onFavorite: {
            root.favorite();
        }
    }

    Item {
        anchors.top: topToolbar.visible ? topToolbar.bottom : parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10


        Category {
            id: category

            anchors.top: parent.top
            anchors.left: parent.left
            width: 200
            height: 20

            model: CategoriesStore.category(root.categoryId);

            onClicked: {
                loader.loadSelectActivity();
            }
        }

        TextField {
            id: title

            anchors.top: category.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.topMargin: 10

            text: root.item ? root.item.title : ""
            placeholderText: "title"
        }

        Rectangle{
            width: parent.width

            anchors.top: title.bottom
            anchors.bottom: buttonsRow.top
            anchors.left: parent.left
            anchors.right: parent.right

            anchors.topMargin: 10
            anchors.bottomMargin: 10

            border.color: root.textFieldBgColor


            TextArea {
                id: textEdit

                text: root.item ? root.item.text : ""

                anchors.fill: parent
            }
        }

        Row {
            id: buttonsRow

            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            height: 40

            property int buttonWidth: parent.width / 3
            spacing: parent.width - (buttonWidth * 2)

            Button {
                id: cancelButton

                text: qsTr("Cancel")
                onClicked: {
                    root.cancel();
                }
            }

            Button {
                id: saveButton

                text: qsTr("Save")
                onClicked: {
                    root.saveItem(root.categoryId, title.text, textEdit.text);
                }
            }
        } // ROW
    }//Item



    Component {
        id: selectCategoriesActivityComponent

        SelectCategory {
            anchors.fill: parent

            onCancel: {

            }

            onDone: {
                if (root.item) {
//                    ToDoStore.setCategory(root.item.id, category.id);
                    root.categoryId = category.id
                }

                loader.clean();
            }
        }
    }

    Loader {
        id: loader

        anchors.fill: parent

        function loadSelectActivity() {
            sourceComponent = selectCategoriesActivityComponent
        }
        function clean() {
            sourceComponent = null;
        }
    }


}//Item
