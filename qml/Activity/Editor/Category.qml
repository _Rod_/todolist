import QtQuick 2.12
import QtQuick.Controls 2.0

Item {
    id: root

    property var model: null

    signal clicked()

    Rectangle {
        id: content
        width: textMetrics.width + 20
        height: textMetrics.height + 10

        radius: height / 2

        border.color: model ? model.color : "grey"
        border.width: 1

        Text {
            id: name

            anchors.centerIn: parent

            text: textMetrics.elidedText
        }

        TextMetrics {
            id: textMetrics
            elide: Text.ElideMiddle
            elideWidth: 300
            text: model ? model.name : "uncategorised"
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            console.log("Clicked on category")

            root.clicked();
        }
    }
}
