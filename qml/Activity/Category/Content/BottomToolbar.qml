import QtQuick 2.0
import Style 1.0
import QtQuick.Controls 2.12

import "../../../Components"
import "../../../Style"

ToolBar {
    id: root

    property bool renameEnabled: false
    property bool removeEnabled: false
    property bool changeColorEnabled: false

    signal setColor();
    signal rename();
    signal remove();

    Row {
        height: childrenRect.height
        anchors.centerIn: parent

        spacing: 50

        ToolButton {
            visible: root.changeColorEnabled
            display: AbstractButton.TextUnderIcon
            icon.source: Assets.eye
            text: "Category color"
            onClicked: {
                root.setColor();
            }
        }
        ToolButton {
            visible: root.renameEnabled
            display: AbstractButton.TextUnderIcon
            icon.source: Assets.editIcon
            text: "Rename"
            onClicked: {
                root.rename();
            }
        }

        ToolButton {
            visible: root.removeEnabled
            display: AbstractButton.TextUnderIcon
            icon.source: Assets.delIcon
            text: "Delete"

            onClicked: {
                root.remove();
            }
        }
    }
}
