import QtQuick 2.0
import QtQuick.Controls 2.0
import Style 1.0
import QtQuick.Controls.Styles 1.4

import "../../../Components"
import "../../../Style"

 ToolBar {
    id: root

    property string title: "Manage categories"
    property bool isEditMode: false

    signal back();
    signal edit();
    signal cancel();

    Rectangle {
        anchors.fill: parent

        id: background
        color: "red"
        opacity: 0.5
        visible: false
    }


    ToolButton {
        id: backButton

        anchors.margins: 10
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter

        icon.source: Assets.back

        onClicked: root.back()
    }


    Text {
        id: titleText

        anchors.margins: 10
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: backButton.right
        anchors.right: editButton.left

        text: root.title
    }


    ToolButton {
        id: editButton

        anchors.margins: 10
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter

//        icon.source: Assets.editIcon
        text: root.isEditMode ? "Cancel" : "Edit"

        onClicked: {
            if (root.isEditMode) {
                root.cancel();
            } else {
                root.edit();
            }
        }
    }
}
