import QtQuick 2.12
import QtQuick.Controls 2.12
import Style 1.0

import "../../Style"
import "./Content"
import "../../Components"

Rectangle {
    id: root

    readonly property var availableColors: CategoriesStore.colors
    property bool editModeActive: false
    property var selectedCategories: []

    property bool renameEnabled: false
    property bool removeEnabled: false
    property bool changeColorEnabled: false

    signal cancel();
    signal done(var category);

    onEditModeActiveChanged: {
        if (root.editModeActive)
            return;

        selectedCategories = [];
        root.updateBottomToolbarState();
        renamePopup.close();
        changeColorPopup.close();
    }

    function updateBottomToolbarState() {
        var numOfCats = root.selectedCategories.length;
        root.renameEnabled = numOfCats === 1;
        root.removeEnabled = numOfCats > 0;
        root.changeColorEnabled = numOfCats > 0;
    }

    Drawer {
        id: drawer

        edge: Qt.TopEdge

        width: root.width
        height: root.height * 0.5

        property color selectedColor

        Column {
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.margins: 10
            spacing: 10

            Label {
                text: "Add category"
            }

            TextField {
                id: newCategoryName
                placeholderText: "Name"
            }

            Label {
                text: "Color"
            }

            ColorsPalette {
                colors: root.availableColors
                selectedColor: root.availableColors.length > 0 ? root.availableColors[0] : "white"
                onSelected: {
                    drawer.selectedColor = color;
                }
            }

            Row {
                spacing: 10

                Button {
                    text: "Cancel"
                    onClicked: {
                        drawer.close();
                    }
                }

                Button {
                    text: "Done"
                    onClicked: {
                        console.log("NEW CATEGORY INFO: ", newCategoryName.text, " ", drawer.selectedColor);
                        CategoriesStore.addCategory(newCategoryName.text, drawer.selectedColor);
                        drawer.close();
                        newCategoryName.text = "";
                    }
                }
            }
        }
    }


    Popup {
        id: renamePopup

        property string text

        anchors.centerIn: parent

        width: parent.width - 20
        height: 200

        modal: true
        focus: true

        background: Rectangle {
            anchors.fill: parent
            radius: 15
        }

        Column {
            spacing: 10
            Label {
                text: "Rename category"
            }

            TextField {
                id: newName
                text: renamePopup.text
            }

            Row {
                spacing: 10
                Button {
                    text: "Cancel"
                    onClicked: {
                        console.log("Clicked cancel")
                        root.editModeActive = false;
                    }
                }
                Button {
                    text: "Rename"
                    onClicked: {
                        console.log("Clicked rename")
                        if (root.selectedCategories.length != 1) {
                            console.log("Group renaming is not supported")
                            return;
                        }

                        CategoriesStore.changeCategoryName(root.selectedCategories[0], newName.text);
                        root.editModeActive = false;
                    }
                }
            }
        }
    }

    Popup {
        id: changeColorPopup

        anchors.centerIn: parent
        width: parent.width - 20
        height: 200

        modal: true
        focus: true

        property color selectedColor

        background: Rectangle {
            anchors.fill: parent
            radius: 15
        }

        Column {
            spacing: 10
            Label {
                text: "Change category color"
            }

            ColorsPalette {
                id: colorsPalette
                colors: root.availableColors
                selectedColor: changeColorPopup.selectedColor
            }

            Row {
                spacing: 10
                Button {
                    text: "Cancel"
                    onClicked: {
                        root.editModeActive = false;
                    }
                }
                Button {
                    text: "Save"
                    onClicked: {
                        for (var i = 0; i < root.selectedCategories.length; i++) {
                            CategoriesStore.changeCategoryColor(root.selectedCategories[i], colorsPalette.selectedColor);
                        }
                        root.editModeActive = false;
                    }
                }
            }
        }
    }

    TopToolbar {
        id: topToolbar

        height: Sizes.topToolbarHeight
        width: parent.width

        anchors.left: parent.left
        anchors.top: parent.top

        isEditMode: root.editModeActive

        onEdit: {
            root.editModeActive = true;
        }

        onCancel: {
            root.editModeActive = false;
        }

        onBack: {
            root.cancel();
        }
    }

    ListView {
        anchors.top: topToolbar.visible ? topToolbar.bottom : topToolbar.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: bottomToolbar.visible ? bottomToolbar.top : parent.bottom

        anchors.topMargin: 20

        model: CategoriesStore.categories
        delegate: Rectangle {
            id: delegate

            property bool selectionAllowed: root.editModeActive
            onSelectionAllowedChanged: {
                if (!selectionAllowed)
                    selected = false;
            }

            property bool selected: false

            function _itemSelected(selected) {
                if (selected)
                    root.selectedCategories.push(modelData.id);
                else {
                    const index = root.selectedCategories.indexOf(modelData.id);
                    if (index > -1) {
                        root.selectedCategories.splice(index, 1);
                    }
                }

                root.updateBottomToolbarState();
            }

            width: parent.width
            height: 40

            border.color: "black"
            border.width: selected ? 1 : 0

//            visible: !root.editModeActive

            Rectangle {
                id: color

                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.verticalCenter: parent.verticalCenter

                width: 15
                height: width
                radius: height

                color: modelData.color
            }

            Text {
                id: name

                anchors.left: color.right
                anchors.leftMargin: 10
                anchors.verticalCenter: parent.verticalCenter

                text: modelData.name
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (root.editModeActive) {
                        delegate.selected = !delegate.selected;
                        delegate._itemSelected(delegate.selected);
                    } else {
                        root.done(modelData);
                        drawer.close();
                    }
                }

                onPressAndHold: {
                    if (!root.editModeActive)
                        root.editModeActive = true;
                    delegate.selected = !delegate.selected;
                    delegate._itemSelected(delegate.selected);
                }
            }
        }

        footer: Rectangle {
            width: parent.width
            height: 20

            visible: !root.editModeActive

            Rectangle {
                id: addImage

                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.verticalCenter: parent.verticalCenter

                width: 15
                height: width

                Image {
                    anchors.fill: parent
                    source: Assets.addIcon
                }
            }

            Text {
                anchors.left: addImage.right
                anchors.leftMargin: 10
                anchors.verticalCenter: parent.verticalCenter

                text: "Add item"

            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    drawer.open();
                }
            }
        }
    }

    BottomToolbar {
        id: bottomToolbar

        anchors.bottom: parent.bottom
        anchors.left: parent.left

        width: parent.width
        height: Sizes.bottomToolbarHeight

        visible: root.renameEnabled || root.removeEnabled || root.changeColorEnabled

        renameEnabled: root.renameEnabled
        removeEnabled: root.removeEnabled
        changeColorEnabled: root.changeColorEnabled

        onSetColor: {
            if (root.selectedCategories.length > 0)
            {
                changeColorPopup.selectedColor = CategoriesStore.category(root.selectedCategories[0]).color;
                changeColorPopup.open();
            }
        }

        onRename: {
            if (root.selectedCategories.length == 1) {
                renamePopup.text = CategoriesStore.category(root.selectedCategories[0]).name;
                renamePopup.open();
            }
        }

        onRemove: {
            CategoriesStore.deleteCategories(root.selectedCategories);
            root.editModeActive = false;
        }

        Behavior on height {
            NumberAnimation {duration: 650; easing.type: Easing.InOutQuad}
        }
    }

}
