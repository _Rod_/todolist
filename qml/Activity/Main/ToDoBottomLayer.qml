import QtQuick 2.12
import QtQuick.Controls 2.12

import Style 1.0 // qmldir
import Gestures 1.0

import "../../Components"
import "../../CppTypes"

Rectangle {
    id: root

    property color textFieldBgColor: "#E0E0E0"
    property string textFieldPlaceHolderText: "I want to..."
    property bool gesturesEnabled: false
    readonly property int controlHeight: 35

    signal activated(var focus);
    signal addItem (var text);

    function appendItem(text) {
        if (text.length > 0){
            root.addItem(text);
            textEdit.clear();
            textEdit.focus = false;
        }
    }

    MouseGestureFilter {
        id: mouseGestureFilter

        enabled: root.gesturesEnabled

        onSwiped: {
            if (direction === Gesture.ToLeft)
                console.log("ToLeft");
            else if (direction === Gesture.ToRight)
                console.log("ToRight");
            else if (direction === Gesture.ToUp)
                console.log("ToUp");
            else if (direction === Gesture.ToDown) {
                console.log("ToDown")
                textEdit.focus = !textEdit.focus;
            }

        }
    }

    TextField {
        id: textEdit

        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.right: addNewItemButton.left
        anchors.margins: 10

        background: Rectangle {
            id: bg
            radius: 20
            color: root.textFieldBgColor
        }

        placeholderText: root.textFieldPlaceHolderText
        leftPadding: bg.radius

        height: root.controlHeight

        onFocusChanged: {
            root.activated(focus)
        }

        onEditingFinished: {
            root.appendItem(textEdit.text);
        }
    }

    ToDoRoundButton {
        id: minimizeButton

        visible: false
        anchors.verticalCenter: parent.verticalCenter

        imageSource: Assets.addIcon
        onClicked: {
            textEdit.focus = !textEdit.focus;
            state: "opened"
        }
    }

    ToDoRoundButton {
        id: addNewItemButton

        height: root.controlHeight

        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.verticalCenter: parent.verticalCenter

        imageSource: Assets.addIcon
        onClicked: {
            root.appendItem(textEdit.text);
        }
    }

    states: [
        State {
            name: "opened"

            PropertyChanges {
                target: addNewItemButton;
                restingColor: "lightgreen";
                anchors.margins: 10
                width: root.width / 3
            }
            AnchorChanges {
                target: addNewItemButton

                anchors.bottom: parent.bottom
                anchors.right: undefined
                anchors.verticalCenter: undefined
                anchors.horizontalCenter: parent.horizontalCenter;
            }


            PropertyChanges {
                target: textEdit;
            }

            AnchorChanges {
                target: textEdit

                anchors.top: parent.top
                anchors.right: parent.right
                anchors.bottom: addNewItemButton.top
                anchors.verticalCenter: undefined
            }
        },
        Transition {
            from: "opened"
            to: ""
            AnchorAnimation {
                targets: [textEdit, addNewItemButton]
                duration: 1100
             }
        }
    ]

    //    } // ROW
} // rectangle
