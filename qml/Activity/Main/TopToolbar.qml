import QtQuick 2.0
import QtQuick.Controls 2.0
import Style 1.0
import QtQuick.Controls.Styles 1.4

import "../../Components"
import "../../Style"

 ToolBar {
    id: root

    signal hamburgerClicked();
    signal search();

    signal sort();
    signal edit();
    signal view();

    Rectangle {
        anchors.fill: parent

        id: background
        color: "red"
        opacity: 0.5
        visible: false
    }


    ToolButton {
        id: hamburgerMenuButton

        anchors.margins: 10
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter

        icon.source: Assets.hamburger

        onClicked: root.hamburgerClicked()
    }


    Text {
        id: title

        anchors.margins: 10
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: hamburgerMenuButton.right
        anchors.right: searchButton.left

        text: "All notes"
    }


    ToolButton {
        id: searchButton

        anchors.margins: 10
        anchors.right: moreButton.left
        anchors.verticalCenter: parent.verticalCenter

        icon.source: Assets.search
    }


    ToolButton {
        id: moreButton

        anchors.margins: 10
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter

        icon.source: Assets.more
//         text: qsTr("⋮")

        onClicked: {
            contextMenu.open();
        }
    }

    Menu {
        id: contextMenu

        x : moreButton.x
        y : moreButton.y

        MenuItem {
            text: 'edit'
            onTriggered: console.log("edit triggered")
        }
        MenuItem {
            text: 'sort'
            onTriggered: {
                root.sort();
                console.log("sort triggered")
            }
        }
        MenuItem {
            text: 'view'
            onTriggered: console.log("view triggered")
        }
    }
}
