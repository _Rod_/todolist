import QtQuick 2.0
import Style 1.0 // qmldir
import ShowItemsType 1.0

import "../../Components"
import "../../Style"

Item {
    id: root

    property var store: ToDoStore
    property var categoriesStore: CategoriesStore
    property var selectedItemsIdx: []
    property bool selectionModeActive: false

    signal openMainMenu();
    signal addItem();
    signal editItem();

    Rectangle {
        id: activityBackground

        anchors.fill: parent
        color: Colors.activityBgColor
    }


    TopToolbar {
        id: topToolbar

        height: Sizes.topToolbarHeight
        width: parent.width

        anchors.left: parent.left
        anchors.top: parent.top

        onHamburgerClicked: {
            root.openMainMenu();
        }

        onSort: {
            ToDoStore.sort();
        }
    }

    ListView {
        id: listView

        anchors.top: topToolbar.visible ? topToolbar.bottom : parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: bottomToolbar.visible ? bottomToolbar.top : parent.bottom

        anchors.margins: 10
        anchors.bottomMargin: 0

        spacing: Sizes.todoItemSpacing
        clip: true

        model: store ? store.items : null
        delegate: ToDoListItemDelegate {
//            readonly property int itemIndex: index

            function _itemSelected(selected) {
                if (selected)
                    root.selectedItemsIdx.push(modelData.id);
                else {
                    const index = root.selectedItemsIdx.indexOf(modelData.id);
                    if (index > -1) {
                        root.selectedItemsIdx.splice(index, 1);
                    }
                }

                selectionModeActive = selectedItemsIdx.length > 0;
            }

            color: Colors.todoItemBgColor
            height: Sizes.todoItemHeight
            radius: Sizes.todoItemRadius

            title: modelData.title
            text: modelData.text
            date: modelData.date
            isFavorite: modelData.favorite
            categoryColor: {
                if (categoriesStore)
                    var cat = categoriesStore.category(modelData.category);
                    if (cat)
                        return cat.color;

                return "grey";
            }

            onEditClicked: {
                if (selectionModeActive) {
                    selected = !selected;
                     _itemSelected(selected);
                    return;
                }

                store.editItem(modelData.id);
                root.editItem();
            }

            onItemSelected: {
                _itemSelected(selected);
            }
        }
    }

    ToDoRoundButton {
        id: addNewItemButton
        width: 50
        height: width

        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 20

        restingColor: "#FF6660"

        visible: !root.selectionModeActive && ToDoStore.showItems == ShowItemsType.AllNotes

        imageSource: Assets.addIcon
        onClicked: {
            root.addItem();
        }
    }


    ToDoRoundButton {
        size: 50

        anchors.right: addNewItemButton.right
        anchors.top: addNewItemButton.top

        onClicked: ToDoStore.clearRecycleBin();
        imageSource: Assets.delIcon

        visible: ToDoStore.showItems == ShowItemsType.RecycleBin
    }

    BottomToolbar {
        id: bottomToolbar

        anchors.bottom: parent.bottom
        anchors.left: parent.left

        width: parent.width
        height: Sizes.bottomToolbarHeight

        visible: root.selectionModeActive

        onRemove: {
            ToDoStore.removeSelectedItems(root.selectedItemsIdx);
            root.selectedItemsIdx = [];
            selectionModeActive = false;
        }

        Behavior on height {
           // NumberAnimation {duration: 650; easing.type: Easing.InOutQuad}
        }
    }

}
