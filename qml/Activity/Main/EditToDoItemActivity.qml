import QtQuick 2.12
import QtQuick.Controls 2.12

import "../../Components"

Item {
    id: root

    property color textFieldBgColor: "#E0E0E0"
    property string text

    signal saveItem (var text);
    signal cancel();

    Item {
        anchors.fill: parent
        anchors.margins: 10

        Rectangle{
            width: parent.width
            anchors.top: parent.top
            anchors.bottom: buttonsRow.top
            anchors.bottomMargin: 10

            border.color: root.textFieldBgColor

            TextArea {
                id: textEdit

                text: root.text
                anchors.fill: parent
            }
        }

        Row {
            id: buttonsRow

            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            height: 40

            property int buttonWidth: parent.width / 3
            spacing: parent.width - (buttonWidth * 2)

            Button {
                id: cancelButton

                text: qsTr("Cancel")
                onClicked: {
                    root.cancel();
                }
            }

            Button {
                id: saveButton

                text: qsTr("Save")
                onClicked: {
                    root.saveItem(textEdit.text);
                }
            }
        } // ROW
    }// Item
}// Item id:root
