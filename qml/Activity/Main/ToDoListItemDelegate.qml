import QtQuick.Controls 2.12
import QtQuick 2.12

import "../../Components"
import "../../Style"

Rectangle {
    id: root

    property string title
    property string text
    property string date
    property color categoryColor
    property bool isFavorite: false

    property bool selected: false

    signal editClicked();
    signal removeClicked();
    signal itemSelected(var selected);

    width: parent.width
    height: 40

    border.color: root.selected ? "black" : "transparent"

    MouseArea {
        anchors.fill: parent

        Rectangle {
            anchors.fill: parent
            opacity: .5
            color: "red"
            visible: false
        }

        onPressAndHold: {
            root.selected = !root.selected;
            root.itemSelected(root.selected);
        }

        onClicked: {
            root.editClicked();
        }
    }


    Item {
        id: content

        anchors.fill: parent

        Item {
            id: category

            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: 25

            Rectangle {
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.topMargin: 18

                width: 10
                height: 10
                radius: height / 2
                color: root.categoryColor
            }
        }

        Item {
            id: data

            anchors.top: parent.top
            anchors.left: category.right
            anchors.bottom: parent.bottom
            anchors.right: parent.right

            Text {
                id: title

                anchors.top: parent.top
                anchors.left: parent.left
                anchors.margins: 15

                font.bold: true
                text: root.title
                visible: text.length > 0
            }

            Text {
                id: itemContent

                anchors.top:  title.visible ? title.bottom : title.top
                anchors.bottom: info.top
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: 15
                anchors.rightMargin: 15

                text: root.text

                clip: true
            }

            Row {
                id: info

                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.margins: 15
                spacing: 10

                Text {
                    text: root.date
                    color: "grey"
                }

                Image {
                    id: favoriteState
                    source: Assets.favorite

                    width: 10
                    height: 10
                    visible: root.isFavorite
                }
            }
        }
    }
}
