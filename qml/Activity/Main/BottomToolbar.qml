import QtQuick 2.0
import Style 1.0
import QtQuick.Controls 2.12

import "../../Components"
import "../../Style"

ToolBar {
    id: root

    signal remove();
    signal share();
    signal lock();
    signal move();

//    Rectangle {
//        anchors.fill: parent
//        color: Colors.bottomToolbarBg
////        opacity: 0.4
//    }

    Row {
        height: childrenRect.height
        anchors.centerIn: parent

        spacing: 50

        ToolButton {
            display: AbstractButton.TextUnderIcon
            icon.source: Assets.move
            text: "move"
            onClicked: {
                root.move();
            }
        }
        ToolButton {
            display: AbstractButton.TextUnderIcon
            icon.source: Assets.locked
            text: "lock"
            onClicked: {
                root.lock();
            }
        }
        ToolButton {
            display: AbstractButton.TextUnderIcon
            icon.source: Assets.share
            text: "share"
            onClicked: {
                root.share();
            }
        }

        ToolButton {
            display: AbstractButton.TextUnderIcon
            icon.source: Assets.delIcon
            text: "delete"

            onClicked: {
                root.remove();
            }
        }
    }
}
