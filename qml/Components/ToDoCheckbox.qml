import QtQuick 2.0

Item {
    id: root

    property alias text: textItem.text
    property alias font: textItem.font

    property bool checked: false

    signal clicked (var checkState)

    implicitHeight: 30

    Item {
        anchors.fill: parent

        Rectangle {
            id: checkboxItem

            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter

            height: root.height
            width: height
            radius: height / 2

            color: "white"
            border.color: "black"

            Rectangle {
                id: internalRectangle

                width: parent.width - 10
                height: width
                radius: height / 2

                anchors.centerIn: parent

                color: "blue"

                opacity: root.checked
                visible: root.checked
                Behavior on opacity{
                    NumberAnimation {duration: 150; easing.type: Easing.InOutBounce}
                }
            }

            MouseArea {
                anchors.fill: parent

                onClicked: {
                    root.clicked(!root.checked)
                }
            }
        }

        Text {
            id: textItem

            anchors.leftMargin: 10
            anchors.left: checkboxItem.right
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter

            clip: true
        }
    }
}
