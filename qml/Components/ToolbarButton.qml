import QtQuick 2.0

Item {
    id: root

    property alias imageSource: image.source
    property alias text: text.text
    property color restingColor: "#4EA9DE"
    property color hoverColor: "#94CAEA"
    property int size: 35

    signal clicked();

    implicitWidth: size
    implicitHeight: size

    Image {
        id: image

        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter

        height: parent.height / 2
        width: height
    }
    Text {
        id: text

        anchors.top: image.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true

        onClicked: {
            root.clicked();
        }

        onEntered: {
            root.color = root.hoverColor
        }

        onExited: {
            root.color = root.restingColor
        }
    }
}
