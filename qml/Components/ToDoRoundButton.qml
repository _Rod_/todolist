import QtQuick 2.0

Rectangle {
    id: root

    property alias imageSource: image.source

    property color restingColor: "#4EA9DE"
    property color hoverColor: "#94CAEA"
    property int size: 35

    color: restingColor
    opacity: enabled ? 1 : 0.3

    signal clicked();

    implicitWidth: size
    implicitHeight: size

    radius: height / 2

    border.color: "black"

    Image {
        id: image
        anchors.centerIn: parent

        height: parent.height / 2
        width: height
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true

        onClicked: {
            root.clicked();
        }

        onEntered: {
            root.color = root.hoverColor
        }

        onExited: {
            root.color = root.restingColor
        }
    }
}
