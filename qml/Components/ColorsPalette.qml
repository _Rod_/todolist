import QtQuick 2.0

Item {
    id: root

    implicitWidth: childrenRect.width
    implicitHeight: childrenRect.height

    property var colors
    property color selectedColor

    signal selected(var color);

    Row {
        spacing: 10

        Repeater {
            model: root.colors

            delegate: Rectangle {
                width: 25
                height: width
                radius: height

                color: modelData

                border.color: "black"
                border.width: color == root.selectedColor ? 2 : 0

                MouseArea {
                    anchors.fill: parent;
                    onClicked: {
                        root.selectedColor = parent.color;
                        root.selected(parent.color);
                    }
                }
            }
        }
    }
}
