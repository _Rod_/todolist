import QtQuick 2.0
import Gestures 1.0

MouseGestureFilterCpp {
    id: root

    anchors.fill: parent

    MouseArea {
        anchors.fill: parent

        preventStealing: true

        onPressed: {
            root.mousePressEvent(mouse.x, mouse.y);
        }

        onPositionChanged: {
            root.mousePositionChangedEvent(mouse.x, mouse.y)
        }

        onReleased: {
            root.mouseReleaseEvent(mouse.x, mouse.y)
        }
    }

}
