pragma Singleton

import QtQuick 2.0

QtObject {
    readonly property int todoItemHeight: 110
    readonly property int todoItemRadius: 15
    readonly property int todoItemSpacing: 15

    readonly property int topToolbarHeight: 60
    readonly property int bottomToolbarHeight: 60

    readonly property int topToolbarButtonHeight: 40
}
