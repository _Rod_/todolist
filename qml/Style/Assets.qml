pragma Singleton

import QtQuick 2.0

QtObject {
    id:root

    readonly property string addIcon: "qrc:/images/icons_add.svg"
    readonly property string delIcon: "qrc:/images/icons_delete.svg"
    readonly property string editIcon: "qrc:/images/icons_edit.svg"

    readonly property string share: "qrc:/images/icon-share.svg"
    readonly property string locked: "qrc:/images/icons_locked.svg"
    readonly property string unlocked: "qrc:/images/icons_unlocked.svg"
    readonly property string move: "qrc:/images/icons_back.svg"
    readonly property string back: "qrc:/images/icons_back.svg"

    readonly property string search: "qrc:/images/icons_search.svg"
    readonly property string hamburger: "qrc:/images/icon-hamburger.svg"
    readonly property string gear: "qrc:/images/icon-gear.svg"
    readonly property string more: "qrc:/images/icon-more.svg"

    readonly property string favorite: "qrc:/images/icons_favorite.svg"
    readonly property string nonfavorite: "qrc:/images/icons_unfavorite.svg"
    readonly property string notes: "qrc:/images/notes.svg"
    readonly property string bookmark: "qrc:/images/bookmark.svg"

    readonly property string template: "qrc:/images/icon-template.svg"
    readonly property string eye: "qrc:/images/eye.svg"
}
