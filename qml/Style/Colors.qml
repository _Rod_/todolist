pragma Singleton

import QtQuick 2.0

QtObject {
    readonly property color activityBgColor: "#F3F3F3" //"#ECECEC"
    readonly property color todoItemBgColor: "#FFFFFF"

    readonly property color bottomToolbarBg: "#CCCCCC"

    readonly property color mainMenuBgColor: "#FFFFFF"
}
