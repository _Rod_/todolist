import QtQuick 2.0
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import Style 1.0

import "Activity/Main"
import "Activity/Editor"
import "Activity/Category"
import "Menu"

Window {
    id: root

    visible: true
    width: 400
    height: 600
    title: qsTr("ToDoList")


    function showMainMenu() {
        mainMenu.open();
    }


    Component {
        id: mainActivityComponent

        MainActivity {
            id: mainActivity

            anchors.fill: parent

            onAddItem: {
                loader.loadEditorActivity();
            }

            onEditItem: {
                loader.loadEditorActivity();
            }

            onOpenMainMenu: {
                showMainMenu();
            }
        }
    }

    Component {
        id: editToDoItemActivityComponent

        EditToDoItemActivity {
            id: editToDoItemActivity

            anchors.fill: parent

            item: ToDoStore.currentItem

            onSaveItem: {
                ToDoStore.saveEditedItem(category, title, text);
                loader.loadMainActivity();
            }

            onCancel: {
                ToDoStore.cancelEdit();
                loader.loadMainActivity();
            }

            onRemove: {
                ToDoStore.removeEditedItem();
                loader.loadMainActivity();
            }

            onFavorite: {
                ToDoStore.makeFavoriteEditedItem();
            }

            onLock: {
                ToDoStore.lockEditedItem();
            }
        }
    }

    Component {
        id: categoriesActivityComponent

        SelectCategory {
            anchors.fill: parent

            onCancel: {
                loader.loadMainActivity();
            }

            onDone: {
                loader.loadMainActivity();
            }
        }
    }

    Loader {
        id: loader

        function loadMainActivity() {
            sourceComponent = mainActivityComponent;
        }
        function loadEditorActivity() {
            sourceComponent = editToDoItemActivityComponent;
        }
        function loadCategoriesActivity() {
            sourceComponent = categoriesActivityComponent;
        }

        anchors.left: mainMenu.right
        anchors.top: parent.top
        width: parent.width
        height: parent.height

        transform: Translate {
            x: mainMenu.position * mainMenu.width
        }

        Component.onCompleted: {
            loadMainActivity();
//            loadCategoriesActivity();
        }
    }

    MainMenu {
        id: mainMenu

        width: 0.66 * root.width
        height: root.height
        color: Colors.mainMenuBgColor

        onShowNotes: {
            ToDoStore.showItems = type;
            close();
        }

        onManageCategories: {
            close();
            loader.loadCategoriesActivity();
        }
    }
}
