#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "Stores/ToDoStore.h"
#include "Stores/CategoriesStore.h"
#include "QmlTypes/MouseGestureFilter.h"


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    CategoriesStore categoriesStore;

//    categoriesStore.addCategory("test", QColor(233,233,233));
//    categoriesStore.addCategory("tmp", QColor(100,100,233));

    ToDoStore toDoStore;

    engine.rootContext()->setContextProperty("ToDoStore", &toDoStore);
    engine.rootContext()->setContextProperty("CategoriesStore", &categoriesStore);

    qmlRegisterSingletonType(QUrl("qrc:/../qml/Style/Assets.qml"), "Style", 1, 0, "Assets");
    qmlRegisterSingletonType(QUrl("qrc:/../qml/Style/Sizes.qml"), "Style", 1, 0, "Sizes");
    qmlRegisterSingletonType(QUrl("qrc:/../qml/Style/Colors.qml"), "Style", 1, 0, "Colors");

    qmlRegisterType<MouseGestureFilter>("Gestures", 1, 0, "MouseGestureFilterCpp");
    qmlRegisterUncreatableType<MouseSwipeGesture>("Gestures", 1, 0, "Gesture", "Cannot create Gesture type in QML");

    qmlRegisterUncreatableType<ShowItemsType>("ShowItemsType", 1, 0, "ShowItemsType", "Cannot create ShowItemsType type in QML");


    const QUrl url(QStringLiteral("qrc:/../qml/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
