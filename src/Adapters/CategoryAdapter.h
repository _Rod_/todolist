#pragma once

#include <QObject>
#include <QString>
#include <QColor>

struct CategoryAdapter
{
    Q_GADGET

    Q_PROPERTY(int id MEMBER id)
    Q_PROPERTY(QString name MEMBER name)
    Q_PROPERTY(QColor color MEMBER color)
    Q_PROPERTY(bool editable MEMBER editable)

public:
    int id = 0;
    QString name;
    QColor color;
    bool editable = true;

    bool operator==(const CategoryAdapter& rhs) const
    {
        return id < rhs.id;
    }
};
Q_DECLARE_METATYPE(CategoryAdapter);

inline bool operator==(const CategoryAdapter& lhs, const CategoryAdapter& rhs)
{
    return lhs.id == rhs.id;
}

inline bool operator!=(const CategoryAdapter& lhs, const CategoryAdapter& rhs)
{
    return lhs.id != rhs.id;
}
