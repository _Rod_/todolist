#pragma once

#include <QObject>
#include <QString>
#include <QVariant>
#include "CategoryAdapter.h"
#include <QDebug>

class ToDoItemAdapter
{
    Q_GADGET

    Q_PROPERTY(int id MEMBER id_)
    Q_PROPERTY(QString title MEMBER title_)
    Q_PROPERTY(QString text MEMBER text_)
    Q_PROPERTY(QString date MEMBER date_)
    Q_PROPERTY(bool check MEMBER check_)
    Q_PROPERTY(bool favorite MEMBER favorite_)
    Q_PROPERTY(bool locked MEMBER locked_)
    Q_PROPERTY(int category MEMBER category_)

public:
    int id_ = 0;
    QString title_;
    QString text_;
    QString date_;
    int category_ = 0;
    bool check_ = false;
    bool favorite_ = false;
    bool locked_ = false;
    bool shared_ = false; // should contain list of users this item shared with
    bool deleted_ = false;
    int used_times_ = 0;
};
Q_DECLARE_METATYPE(ToDoItemAdapter);
