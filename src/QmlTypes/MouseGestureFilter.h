#pragma once

#include <QObject>
#include <QQuickItem>
#include <QMouseEvent>
#include <QEvent>
#include <QDebug>
#include <cmath>

class MouseSwipeGesture
{
    Q_GADGET

public:
    enum Direction
    {
        ToLeft,
        ToRight,
        ToUp,
        ToDown,
    };
    Q_ENUM(Direction)
};

////////

class MouseGestureFilter : public QQuickItem
{
    Q_OBJECT

public:
    Q_INVOKABLE void mousePressEvent(int x, int y)
    {
        qDebug() << "Mouse event " << x << ", " << y;
        yStart = y;
        yPrev = y;
        velocity = 0;
        tracing = true;
    }

    Q_INVOKABLE void mousePositionChangedEvent(int x, int y)
    {
        if (!tracing)
            return;

        auto currVelocity = y - yPrev;
        velocity = (velocity + currVelocity) / 2.0;
        yPrev = y;

        auto absVelocity = std::abs(velocity);
        auto absY = std::abs(y);

        // TODO: x should stay in some range as well to avoid diagonal swipes
        if (absVelocity > 15 && absY > height() * 0.2)
        {
            tracing = false;

            if (velocity < 0)
            {
                qDebug() << "SWIPE DOWN-UP DETECTED !!!";
                emit swiped(MouseSwipeGesture::ToUp);
            }
            else
            {
                qDebug() << "SWIPE UP-DOWN DETECTED !!!";
                emit swiped(MouseSwipeGesture::ToDown);
            }
        }
    }

    Q_INVOKABLE void mouseReleaseEvent(int x, int y)
    {
        if (!tracing)
            return;

        tracing = false;
        auto absVelocity = std::abs(velocity);
        auto absY = std::abs(y);

        // TODO: x should stay in some range as well to avoid diagonal swipes
        if (absVelocity > 15 && absY > height() * 0.2)
        {
            tracing = false;

            if (velocity < 0)
            {
                qDebug() << "SWIPE DOWN-UP DETECTED ";
                emit swiped(MouseSwipeGesture::ToUp);
            }
            else
            {
                qDebug() << "SWIPE UP-DOWN DETECTED ";
                emit swiped(MouseSwipeGesture::ToDown);
            }
        }
    }

signals:
    void swiped(int direction);

private:
    bool tracing = false;
    int yPrev = 0;
    int yStart = 0;
    double velocity = 0;
};
