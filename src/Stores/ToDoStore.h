#pragma once

#include <QObject>
#include <QString>
#include <QVariant>
#include <QVariantList>
#include <QDate>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QFile>
#include <QDebug>

#include <set>

#include "Adapters/ToDoItemAdapter.h"


class ShowItemsType
{
    Q_GADGET

public:
    enum Type
    {
        AllNotes,
        Favorites,
        Locked,
        FrequentlyUsed,
        SharedNotes,
        RecycleBin,
    };
    Q_ENUM(Type)
};

////////////////

class ToDoStore : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariantList items READ items NOTIFY itemsChanged)
    Q_PROPERTY(ToDoItemAdapter currentItem READ currentItem NOTIFY currentItemChanged)
    Q_PROPERTY(int showItems WRITE setShowItems READ showItems NOTIFY showItemsChanged)

public:
    void print(const ToDoItemAdapter& inItem)
    {
        qDebug() << inItem.text_ << " " << inItem.check_;
    }

    ToDoStore()
    {
        load();
    }

    void setShowItems(int inType)
    {
        mShowItemsType = (ShowItemsType::Type)inType;
        emit showItemsChanged();
        emit itemsChanged();
    }

    int showItems() const
    {
        return mShowItemsType;
    }

    bool shouldBeVisible(const ToDoItemAdapter& inItem)
    {
        switch (mShowItemsType)
        {
        case ShowItemsType::AllNotes:
            return inItem.deleted_ == false;

        case ShowItemsType::Locked:
            return inItem.locked_;

        case ShowItemsType::Favorites:
            return inItem.favorite_;

        case ShowItemsType::SharedNotes:
            return inItem.shared_;

        case ShowItemsType::RecycleBin:
            return inItem.deleted_;

        case ShowItemsType::FrequentlyUsed:
            return inItem.used_times_ > 10;

//        default:
//            assert(false);
        }

        return false;
    }

    Q_INVOKABLE void setCategory(int inId, int inCategory)
    {
        auto item = getItem(inId);
        item.category_ = inCategory;
        updateItem(inId, item);

        emit currentItemChanged();
    }

    QVariantList items()
    {
        QVariantList tmp;
        for (const auto& item : mItems)
        {
            if (shouldBeVisible(item))
                tmp.push_back(QVariant::fromValue(item));
        }
        return tmp;
    }

    Q_INVOKABLE void addItem(const int inCategoryId, const QString& inTitle, const QString& inText)
    {
        ToDoItemAdapter item;
        item.category_ = inCategoryId;
        item.title_ = inTitle;
        item.text_ = inText;
        item.date_ =  QDate::currentDate().toString();
        item.check_ = false;

        if (mItems.empty())
            item.id_ = 1;
        else {
            item.id_ = mItems.last().id_ + 1;
        }

        mItems.push_back(item);
        emit itemsChanged();
        save();
    }

    Q_INVOKABLE void saveEditedItem(int inCategoryId, QString title, QString text)
    {
        auto& id = mEditedItemId;
        if (id != -1)
        {
            auto item = getItem(id);
            item.category_ = inCategoryId;
            item.title_ = title;
            item.text_ = text;
            updateItem(id, item);
            id = -1;
            save();
            emit itemsChanged();
        }
        else
        {
            addItem(inCategoryId, title, text);
        }
    }

    Q_INVOKABLE void editItem(int inId)
    {
        mEditedItemId = inId;
        auto item = getItem(inId);
        item.used_times_++;
        updateItem(inId, item);
        save();
    }

    Q_INVOKABLE void cancelEdit()
    {
        mEditedItemId = -1;
    }

    Q_INVOKABLE void removeItem(int inId)
    {
        auto item = getItem(inId);
        item.deleted_ = true;
        updateItem(inId, item);
        emit itemsChanged();
        save();
    }

    Q_INVOKABLE void removeSelectedItems(QList<int> inItemsIdx)
    {
        std::set<int> indices(inItemsIdx.begin(), inItemsIdx.end());
        for (auto& elem : mItems)
        {
            if (indices.count(elem.id_))
            {
                elem.deleted_ = true;
            }
        }

        emit itemsChanged();
        save();
    }

    Q_INVOKABLE void removeEditedItem()
    {
        removeItem(mEditedItemId);
    }

    Q_INVOKABLE void clearRecycleBin()
    {
        mItems.erase(std::remove_if(mItems.begin(), mItems.end(), [](const ToDoItemAdapter& item) { return item.deleted_; } ), mItems.end());
        emit itemsChanged();
        save();
    }


    Q_INVOKABLE void lockEditedItem()
    {
        auto item = getItem(mEditedItemId);
        item.locked_ = !item.locked_;
        updateItem(mEditedItemId, item);
        save();
        emit currentItemChanged();
    }

    Q_INVOKABLE void makeFavoriteEditedItem()
    {
        auto item = getItem(mEditedItemId);
        item.favorite_ = !item.favorite_;
        updateItem(mEditedItemId, item);
        save();
        emit currentItemChanged();
    }

    Q_INVOKABLE void checkItem(bool inCheck, int inIndex)
    {
        mItems[inIndex].check_ = inCheck;
        print( mItems[inIndex]);
        save();
        emit itemsChanged();
    }

    Q_INVOKABLE void sort()
    {
        for (int i = 0, j = mItems.size()-1; i < mItems.size() / 2; i++, j--)
        {
            mItems.swap(i, j);
        }
        emit itemsChanged();
    }

    ToDoItemAdapter currentItem()
    {
        return getItem(mEditedItemId);
    }

    ///////////////////

private:
    ToDoItemAdapter getItem(int inId)
    {
        for (const auto& elem : mItems)
        {
            if (elem.id_ == inId)
                return elem;
        }

        return {};
    }

    void updateItem(int inId, const ToDoItemAdapter& inItem)
    {
        for (auto& elem : mItems)
        {
            if (elem.id_ == inId)
            {
                elem = inItem;
                break;
            }
        }
    }


private:
    void save()
    {
        QJsonObject root;
        QJsonArray array;
        for(const auto& item : mItems)
        {
            QJsonObject jsonItem;
            jsonItem["id"] = item.id_;
            jsonItem["title"] = item.title_;
            jsonItem["text"] = item.text_;
            jsonItem["date"] = item.date_;
            jsonItem["check"] = item.check_;
            jsonItem["favorite"] = item.favorite_;
            jsonItem["locked"] = item.locked_;
            jsonItem["deleted"] = item.deleted_;
            jsonItem["shared"] = item.shared_;
            jsonItem["used_times"] = item.used_times_;
            jsonItem["category"] = item.category_;

            array.push_back(jsonItem);
        }
        root["items"] = array;
        QJsonDocument doc;
        doc.setObject(root);

        QFile file(mFilename);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            qDebug() <<"Unable to open file " << mFilename;
            return;
        }

        file.write(doc.toJson());
        file.close();
    }

    void load()
    {
        QFile file(mFilename);
        if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            qDebug() << "Unable to open file " << mFilename;
            return;
        }

        QByteArray data = file.readAll();
        QJsonDocument doc = QJsonDocument::fromJson(data);
        QJsonObject root = doc.object();

        if (!root["items"].isArray())
        {
            qDebug() << "items is not an array";
            return;
        }

        mItems.clear();
        QJsonArray array = root["items"].toArray();
        for (auto element : array)
        {
            auto toDoItemJson = element.toObject();

            ToDoItemAdapter item;
            item.id_ = toDoItemJson["id"].toInt();
            item.title_ = toDoItemJson["title"].toString();
            item.text_ = toDoItemJson["text"].toString();
            item.date_ = toDoItemJson["date"].toString();
            item.check_ = toDoItemJson["check"].toBool();
            item.favorite_ = toDoItemJson["favorite"].toBool();
            item.locked_ = toDoItemJson["locked"].toBool();
            item.deleted_ = toDoItemJson["deleted"].toBool();
            item.shared_ = toDoItemJson["shared"].toBool();
            item.used_times_ = toDoItemJson["used_times"].toInt();
            item.category_ = toDoItemJson["category"].toInt();

            mItems.push_back(item);
        }
        emit itemsChanged();
    }

    /////////////////////

signals:
    void itemsChanged();
    void currentItemChanged();
    void showItemsChanged();

private:
    QList<ToDoItemAdapter> mItems;
    const QString mFilename = "ToDoItems.json";
    int mEditedItemId = -1;
    ShowItemsType::Type mShowItemsType = ShowItemsType::Type::AllNotes;
};
