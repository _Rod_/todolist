#pragma once

#include <QObject>
#include <QList>
#include <QDebug>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include "Adapters/CategoryAdapter.h"

class CategoriesStore : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariantList categories READ categories NOTIFY categoriesChanged)
    Q_PROPERTY(QVariantList colors READ colors NOTIFY colorsChanged)

public:
    CategoriesStore()
    {
        initColors();

        load();
    }

    QVariantList categories()
    {
        QVariantList list;
        for (const auto& elem : mCategories)
        {
            list.push_back(QVariant::fromValue(elem));
        }
        return list;
    }

    Q_INVOKABLE void addCategory(const QString& inName, const QColor& inColor)
    {
        CategoryAdapter category;

        category.name = inName;
        category.color = inColor;

        if (mCategories.empty())
            category.id = 1;
        else
            category.id = mCategories.last().id + 1;

        mCategories.push_back(category);
        emit categoriesChanged();

        save();
    }

    Q_INVOKABLE void editCategory(const int inId, const QString& inName, const QColor& inColor)
    {
        for (auto& elem : mCategories)
        {
            if (elem.id == inId)
            {
                elem.name = inName;
                elem.color = inColor;
            }
        }
        emit categoriesChanged();

        save();
    }

    Q_INVOKABLE void changeCategoryName(const int inId, const QString& inName)
    {
        for (auto& elem : mCategories)
        {
            if (elem.id == inId)
            {
                elem.name = inName;
            }
        }
        emit categoriesChanged();

        save();
    }

    Q_INVOKABLE void changeCategoryColor(const int inId, const QColor& inColor)
    {
        for (auto& elem : mCategories)
        {
            if (elem.id == inId)
            {
                elem.color = inColor;
            }
        }
        emit categoriesChanged();

        save();
    }

    Q_INVOKABLE void deleteCategories(const QList<int>& inIds)
    {
        mCategories.erase(std::remove_if(mCategories.begin(), mCategories.end(), [inIds](const CategoryAdapter& inItem) { return inIds.contains(inItem.id); }),
                        mCategories.end());
        emit categoriesChanged();

        save();
    }

    QVariantList colors()
    {
        QVariantList list;
        for (auto elem : mColors)
        {
            list.push_back(QVariant::fromValue(elem));
        }
        return list;
    }

    Q_INVOKABLE QVariant category(int inId)
    {
        for (auto elem : mCategories)
        {
            if (elem.id == inId)
                return QVariant::fromValue(elem);
        }
        return {};
    }

private:
    void initColors()
    {
        mColors = {
            "red",
            "green",
            "yellow",
            "blue",
            "purple",
            "magenta",
            "cyan",
            "lightgreen",
        };
        emit colorsChanged();
    }


private:
    void save()
    {
        QJsonObject root;
        QJsonArray array;
        for(const auto& item : mCategories)
        {
            QJsonObject jsonItem;
            jsonItem["id"] = item.id;
            jsonItem["name"] = item.name;
            jsonItem["color"] = item.color.name();
            jsonItem["editable"] = item.editable;

            array.push_back(jsonItem);
        }
        root["categories"] = array;
        QJsonDocument doc;
        doc.setObject(root);

        QFile file(mFilename);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            qDebug() <<"Unable to open file " << mFilename;
            return;
        }

        file.write(doc.toJson());
        file.close();
    }

    void load()
    {
        QFile file(mFilename);
        if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            qDebug() << "Unable to open file " << mFilename;
            return;
        }

        QByteArray data = file.readAll();
        QJsonDocument doc = QJsonDocument::fromJson(data);
        QJsonObject root = doc.object();

        if (!root["categories"].isArray())
        {
            return;
        }

        mCategories.clear();
        QJsonArray array = root["categories"].toArray();
        for (auto element : array)
        {
            auto jsonItem = element.toObject();

            CategoryAdapter item;
            item.id = jsonItem["id"].toInt();
            item.name = jsonItem["name"].toString();
            item.color = jsonItem["color"].toString();
            item.editable = jsonItem["editable"].toBool();

            mCategories.push_back(item);
        }
        emit categoriesChanged();
    }

signals:
    void categoriesChanged();
    void colorsChanged();

private:
    QList<CategoryAdapter> mCategories;
    QList<QColor> mColors;
    const QString mFilename = "Categories.json";
};
